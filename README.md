# Ejemplo de TDD con Laravel >= 7.3

Este repositorio es un ejemplo para el desarrollo basado en pruebas con Laravel.
Este documento describe los pasos a realizar, los cuáles están reflejados en
cada commit de la rama main.  

El proyecto de ejemplo consiste en el registro de usuarios en archivo usando
la fachada _Storage_ de Laravel. Las operaciones soportadas son:
* Alta de usuarios.
* Consulta de todos los usuarios registrados.
* Baja de usuarios.

## Pasos a realizar

1. Después de crear el proyecto (git tag = init), cree las pruebas para las
   las funcionalidades a implementar. En el caso de este proyecto se crea una
   prueba de característica para las tres operaciones de usuarios.  
   `php artisan make:test <NombrePrueba>`  
   En este ejemplo:  
   `php artisan make:test UserTest`  
   Si necesita crear pruebas específicas para una funcionalidad, puede crear
   una prueba unitaria agregando la bandera `--unit` al final del nombre de la
   prueba.
2. Defina las pruebas (git tag = tests). Modifique los archivos creados en
   `tests/*/*Test.php` para definir el comportamiento de las pruebas. En TDD las
   pruebas deben comprobar el comportamiento del software en
   construcción/mantenimiento. Para esto es indispensable contar con el diseño
   de al menos las características del software que se van a desarrollar en
   la etapa actual del proyecto. El diseño debe ser candidato a final y no
   debería sufrir modificaciones significativas salvo que se detecten
   malentendidos de los requerimientos y expectativas de los interesados o
   surgan nuevos requerimientos. Las pruebas se escriben **ANTES** que el código
   del software; las pruebas son parte del repositorio de trabajo, **NO** son
   parte del producto, sino que validan su comportamiento.  
   En Laravel, un marco de trabajo para el desarrollo de aplicaciones y sistemas
   basados en Web, las pruebas deben validar que el (los) _servidor(es)_
   procesan las peticiones Web de forma apropiada;  
   - Si se solicita obtener una página web, debe validarse que se obtiene la
     vista correcta.
   - Si se solicita publicar contenido, debe validarse que el estado del
     servidor cambia, alojando el contenido provisto y que se puede consultar
     mediante los mecanismos con los que cuente para ello.
   - Si se borra una publicación, debe validarse que el estado del servidor
     cambia, borrando o marcando la publicación como eliminada y que la
     respuesta de la operación es la esperada (por ejemplo se recibe un código
     200 y una vista específica de confirmación).
   - Etcétera.
   Consulte el archivo `tests/Feature/UserTest.php` de este proyecto de ejemplo
   para ver las pruebas para las 3 funcionalidades de este proyecto.  
   Para ejecutar las pruebas se usa el comando:  
   `php artisan test`  
   Al inicio las pruebas fallan puesto que aún no existen los mecanismos del
   servidor que implementan las características que se prueban. El objetivo
   de cada periodo de desarrollo en TDD es hacer que el software satisfaga las
   pruebas.
3. Se definen las funcionalidades de la aplicación Laravel que
   deben cubrir las pruebas definidas (que deben corresponder)
   con las funcionalidades del servidor.
