<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    public function register(Request $request)
    {
        $request -> validate(['name' => 'required']);
        $userName = $request -> input('name');
        $userFileName = 'users'. DIRECTORY_SEPARATOR . $userName;
        if (Storage::exists($userFileName))
        {
            $userFileContent = Storage::get($userFileName);
            $userFileContent += 1;
        }
        else
        {
            $userFileContent = "1";
        }
        Storage::put($userFileName, $userFileContent);
        return redirect() -> route('userList');
    }

    public function showAll(Request $request)
    {
        $almacenamiento = Storage::disk('local');
        $users = [];
        foreach ($almacenamiento -> files('users') as $user)
        {
            $users[$user] = $almacenamiento -> get($user);
        }
        return view('users.all', ['users' => $users]);
    }

    public function delete(Request $request)
    {
        $request -> validate(['name' => 'required']);
        $userName = $request -> input('name');
        $userFileName = 'users'. DIRECTORY_SEPARATOR . $userName;
        if (Storage::exists($userFileName))
        {
            $userFileContent = Storage::get($userFileName);
            $userFileContent -= 1;
            if ($userFileContent)
            {
                Storage::put($userFileName, $userFileContent);
            }
            else
            {
                Storage::delete($userFileName);
            }
        }
        else
        {
            return back() -> withErrors(
                ['name' => "El usuario $userName no está registrado"]
            );
        }
        return redirect() -> route('userList');
    }

}
