<!DOCTYPE html>
<html lang="es-MX">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Styles -->
        <title>Lista de usuarios</title>
    </head>
    <body>
        <div>
            <table>
                <thead>
                    <th>
                        <td>Nombre</td>
                        <td># Registros</td>
                    </th>
                </thead>
                <tbody>
                    @foreach($users as $user => $num)
                    <tr>
                        <td>{{ $user }}</td>
                        <td>{{ $num }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </body>
</html>
