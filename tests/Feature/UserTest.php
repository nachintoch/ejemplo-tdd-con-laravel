<?php

namespace Tests\Feature;

use Tests\TestCase;

use Illuminate\Support\Facades\Storage;

class UserTest extends TestCase
{

    private $_userName = 'TestUser';
    private $_testUserFileName;

    public function setUp(): void
    {
        parent::setUp();
        $this -> _testUserFileName = 'users'. DIRECTORY_SEPARATOR
            . "{$this->_userName}";
    }

    /**
     * Prueba el registro de usuarios
     *
     * @return void
     */
    public function testRegister()
    {
        Storage::delete($this -> _testUserFileName);
        $response = $this -> post('/user', ['name' => $this -> _userName]);
        $response -> assertRedirect('/users');
        Storage::assertExists($this -> _testUserFileName);
        $userFileContent = Storage::get($this -> _testUserFileName);
        $this -> assertEquals($userFileContent, 1);
    }//testRegister

    /**
     * Prueba la consulta por todos los usuarios.
     *
     * @return void
     */
    public function testGetAll()
    {
        $this -> post('/user', ['name' => $this -> _userName]);
        $response = $this -> get('/users');
        $response -> assertStatus(200);
        $response -> assertViewIs('users.all');
    }//testGetAll

    /**
     * Prueba la baja de usuarios.
     *
     * @return void
     */
    public function testDelete()
    {
        $this -> post('/user', ['name' => $this -> _userName]);
        $originalUserFileContent = Storage::get($this -> _testUserFileName);
        $response = $this -> delete('/user', ['name' => $this -> _userName]);
        $response -> assertRedirect('/users');
        if (Storage::missing($this -> _testUserFileName))
        {
            $newUserFileContent = "0";
        }
        else
        {
            $newUserFileContent = Storage::get($this -> _testUserFileName);
            $this -> assertNotEquals($newUserFileContent, "0");
        }
        $this -> assertLessThan($originalUserFileContent,
            $newUserFileContent);
    }

}
